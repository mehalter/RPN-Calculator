-----
README
-----

== RPN-Calculator ==

Creator : Micah Halter

Version 1.00 13 March 2015

Requires: Java8 (optonal: ANT)

======================================================================
I. Description
--------------

Compiling and Running:

(From ant build.xml)

1. **~$ ant build**
2. **~$ ant Calculator**

(Runnable JAR File)

1. **~$ java -jar Calculator-vXXX.jar**

This is a java based reverse polish notation calculator for my current advanced programming class. We started off building a stack data structure class, and then building this calculator around it.

II. Included
------------

- README.md
- LICENSE
- Calculator-vXXXX.jar
- src


======================================================================
Contact
-------

Question, Comments, and Bugs at:

- micah@mehalter.com
