package com.mehalter.calculator.model;

import com.mehalter.calculator.stack.IStack;
import com.mehalter.calculator.stack.LStack;

public class CalcState {

    private IStack<Double> stack;
    private boolean edit;
    private boolean oped;
    private boolean rad;

    public CalcState() {
        stack = new LStack<Double>();
        edit = false;
        oped = false;
        rad = true;
    }

    public IStack<Double> getStack() {
        return stack;
    }

    public int stackSize() {
        return stack.size();
    }

    public Double popStack() {
        return stack.pop();
    }

    public Double peekStack() {
        return stack.peek();
    }

    public void clearStack() {
        stack.clear();
    }

    public void pushStack(Double i) {
        stack.push(i);
    }

    public boolean getEdit() {
        return edit;
    }

    public boolean getOped() {
        return oped;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public void setOped(boolean oped) {
        this.oped = oped;
    }

    public boolean getRad() {
        return rad;
    }

    public void setRad(boolean rad) {
        this.rad = rad;
    }

}
