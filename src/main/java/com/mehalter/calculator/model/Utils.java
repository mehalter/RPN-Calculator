package com.mehalter.calculator.model;

import java.awt.event.KeyEvent;

public class Utils {

    private CalcState state;
    private TextFields stacks;

    public Utils(CalcState state, TextFields stacks) {
        this.state = state;
        this.stacks = stacks;
    }

    public void refreshStack() {
        int m = 0;
        for (Double i : state.getStack()) {
            if (m < 4) {
                stacks.setText(3 - m, i.toString());
                m++;
            } else
                break;
        }
        for (int i = 3 - state.stackSize(); i >= 0; i -= 1)
            stacks.setText(i, "");
        state.setEdit(true);

    }

    public void ifEditifOp() {

        if (state.getEdit()) {
            if (state.getOped()) {
                state.pushStack(Double.valueOf(stacks.get(4)));
                refreshStack();
            }
            stacks.setText(4, "");
            state.setEdit(false);
        }

    }

    public static boolean validTyped(char e) {
        return (Character.isDigit(e) || (e == KeyEvent.VK_BACK_SPACE)
                || (e == KeyEvent.VK_DELETE) || (e == '.') || (e == '-'));
    }

    public static boolean validEnter(String text) {
        return (text.length() == 1 && (text.contains("-") || text.contains(".")))
                || (text.length() == 2 && text.contains("-."));
    }
}