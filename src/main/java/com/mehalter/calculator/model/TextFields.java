package com.mehalter.calculator.model;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.JTextComponent;

public class TextFields {
    private JTextField[] stacks;

    public TextFields(int i) {
        stacks = new JTextField[i];
        for (int k = 0; k < i; k++) {
            stacks[k] = createTextField();
        }
    }

    public void setLastListener(Utils utils) {
        int i = stacks.length;
        stacks[i - 1].setEditable(true);
        stacks[i - 1].setBackground(Color.LIGHT_GRAY);

        stacks[i - 1].addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                if (!Utils.validTyped(e.getKeyChar()))
                    e.consume();
                else {
                    utils.ifEditifOp();
                    if (e.getKeyChar() == '-') {
                        if (((JTextComponent) e.getSource()).getText().length() != 0)
                            e.consume();
                        else if (((JTextComponent) e.getSource()).getText()
                                .length() == 1
                                && ((JTextComponent) e.getSource()).getText() == "-")
                            e.consume();
                    } else if (e.getKeyChar() == '.')
                        if (((JTextComponent) e.getSource()).getText()
                                .contains("."))
                            e.consume();
                }
            }
        });
    }

    public void setKeyboardShortcuts(JButton[] opKeys, JButton[] leftKeys) {
        stacks[stacks.length - 1].addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                switch (e.getKeyChar()) {
                case '-':
                    if (((JTextComponent) e.getSource()).getText().length() != 0)
                        opKeys[11].doClick();
                    break;
                case '+':
                    opKeys[10].doClick();
                    break;
                case '*':
                    opKeys[7].doClick();
                    break;
                case '/':
                    opKeys[8].doClick();
                    break;
                case 'e':
                    opKeys[6].doClick();
                    break;
                case '^':
                    opKeys[4].doClick();
                    break;
                case 'r':
                    leftKeys[0].doClick();
                    break;
                case 's':
                    leftKeys[1].doClick();
                    break;
                case 'd':
                    leftKeys[2].doClick();
                    break;
                case 'a':
                    leftKeys[3].doClick();
                    break;
                case 'c':
                    leftKeys[4].doClick();
                    break;
                case 'q':
                    System.exit(0);
                    break;
                }
            }
        });
    }

    private static JTextField createTextField() {

        JTextField text = new JTextField();
        text.setHorizontalAlignment(SwingConstants.RIGHT);
        text.setEditable(false);
        text.setFont(new Font("Helvetica", Font.PLAIN, 20));
        return text;

    }

    public void setText(int i, String x) {
        stacks[i].setText(x);
        ;
    }

    public void getFocus() {
        stacks[stacks.length - 1].requestFocus();
    }

    public String get(int i) {
        return stacks[i].getText();
    }

    public JTextField getField(int i) {
        return stacks[i];
    }

}