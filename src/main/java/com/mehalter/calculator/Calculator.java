package com.mehalter.calculator;

import javax.swing.JFrame;

import com.mehalter.calculator.ui.BottomPanel;
import com.mehalter.calculator.ui.LeftPanel;
import com.mehalter.calculator.ui.NumPanel;
import com.mehalter.calculator.ui.OpPanel;
import com.mehalter.calculator.ui.StackPanel;
import com.mehalter.calculator.model.CalcState;
import com.mehalter.calculator.model.TextFields;
import com.mehalter.calculator.model.Utils;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Calculator extends JFrame implements Runnable {

    private static final long serialVersionUID = -2866498891179697972L;

    private TextFields stacks;
    private CalcState state;
    private Utils utils;

    public Calculator() {
        super("RPN Calculator");
        state = new CalcState();
    }

    public void run() {

        setSize(650, 750);
        setResizable(false);
        setLayout(new BorderLayout());
        stacks = new TextFields(5);
        utils = new Utils(state, stacks);
        stacks.setLastListener(utils);
        LeftPanel leftPanel = new LeftPanel(state, utils, stacks);
        OpPanel opPanel = new OpPanel(state, utils, stacks);
        getRootPane().setDefaultButton(opPanel.getKeys()[9]);
        stacks.setKeyboardShortcuts(opPanel.getKeys(), leftPanel.getKeys());
        addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                stacks.getFocus();
            }
        });
        getContentPane().add(new NumPanel(stacks, utils), BorderLayout.CENTER);
        getContentPane().add(opPanel, BorderLayout.EAST);
        getContentPane().add(leftPanel, BorderLayout.WEST);
        getContentPane().add(new StackPanel(stacks), BorderLayout.NORTH);
        getContentPane().add(new BottomPanel(stacks, utils, state),
                BorderLayout.SOUTH);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

}