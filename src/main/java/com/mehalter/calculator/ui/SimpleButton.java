package com.mehalter.calculator.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class SimpleButton extends JButton {

    private static final long serialVersionUID = 8140722804416108071L;

    public SimpleButton(String text, Color color) {
        super(text);
        setOpaque(true);
        setForeground(Color.BLACK);
        setBackground(color);

        addMouseListener(new MouseListener() {
            public void mousePressed(MouseEvent e) {
                setBackground(color.darker());
            }

            public void mouseReleased(MouseEvent e) {
                setBackground(color);
            }

            public void mouseClicked(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });

        setBorder(new CompoundBorder(new LineBorder(Color.WHITE, 2),
                new EmptyBorder(50, 20, 50, 20)));
        setFont(new Font("Helvetica", Font.PLAIN, 20));
    }

    public SimpleButton(String text) {
        this(text, Color.LIGHT_GRAY);
    }

}
