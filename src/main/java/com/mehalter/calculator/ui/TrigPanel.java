package com.mehalter.calculator.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.mehalter.calculator.model.CalcState;
import com.mehalter.calculator.model.TextFields;
import com.mehalter.calculator.model.Utils;

public class TrigPanel extends JPanel {

    private static final long serialVersionUID = 8150202400626370885L;

    public TrigPanel(TextFields stacks, CalcState state, Utils utils) {
        setLayout(new GridLayout(1, 7));
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(0, 50));
        JButton[] knownKeys = new JButton[7];
        knownKeys[0] = new SimpleButton("sec");
        knownKeys[0].addActionListener(new trigListener(knownKeys[0], stacks,
                utils, state));
        knownKeys[1] = new SimpleButton("csc");
        knownKeys[1].addActionListener(new trigListener(knownKeys[1], stacks,
                utils, state));
        knownKeys[2] = new SimpleButton("cot");
        knownKeys[2].addActionListener(new trigListener(knownKeys[2], stacks,
                utils, state));
        knownKeys[3] = new SimpleButton("asin");
        knownKeys[3].addActionListener(new trigListener(knownKeys[3], stacks,
                utils, state));
        knownKeys[4] = new SimpleButton("acos");
        knownKeys[4].addActionListener(new trigListener(knownKeys[4], stacks,
                utils, state));
        knownKeys[5] = new SimpleButton("atan");
        knownKeys[5].addActionListener(new trigListener(knownKeys[5], stacks,
                utils, state));
        knownKeys[6] = new SimpleButton(state.getRad() ? "rad" : "deg");
        knownKeys[6].addActionListener(e -> {
            state.setRad(!state.getRad());
            knownKeys[6].setText(state.getRad() ? "rad" : "deg");
        });
        for (int k = 0; k < knownKeys.length; k++)
            add(knownKeys[k]);
    }
}

class trigListener implements ActionListener {

    private JButton key;
    private TextFields stacks;
    private Utils utils;
    private CalcState state;

    public trigListener(JButton key, TextFields stacks, Utils utils,
            CalcState state) {
        this.key = key;
        this.stacks = stacks;
        this.utils = utils;
        this.state = state;
    }

    public void actionPerformed(ActionEvent e) {
        if (!stacks.get(4).endsWith("E")) {
            Double x = Double.parseDouble(stacks.get(4));
            switch (key.getText()) {
            case "sin":
                x = Math.sin(x);
                break;
            case "cos":
                x = Math.cos(x);
                break;
            case "tan":
                x = Math.tan(x);
                break;
            case "sec":
                x = 1.0 / Math.cos(x);
                break;
            case "csc":
                x = 1.0 / Math.sin(x);
                break;
            case "cot":
                x = 1.0 / Math.tan(x);
                break;
            case "asin":
                x = Math.asin(x);
                break;
            case "acos":
                x = Math.acos(x);
                break;
            case "atan":
                x = Math.atan(x);
                break;
            }
            stacks.setText(4,
                    String.valueOf(state.getRad() ? x : Math.toDegrees(x)));
            utils.refreshStack();
            state.setOped(true);
            state.setEdit(true);
        }
    }

}
