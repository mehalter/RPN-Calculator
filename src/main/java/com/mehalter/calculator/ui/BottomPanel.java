package com.mehalter.calculator.ui;

import java.awt.GridLayout;

import javax.swing.JPanel;

import com.mehalter.calculator.model.CalcState;
import com.mehalter.calculator.model.TextFields;
import com.mehalter.calculator.model.Utils;

public class BottomPanel extends JPanel {

    private static final long serialVersionUID = 5087704112598170173L;

    public BottomPanel(TextFields stacks, Utils utils, CalcState state) {
        this.setLayout(new GridLayout(2, 1));
        this.add(new ConstantsPanel(stacks, state, utils));
        this.add(new TrigPanel(stacks, state, utils));
    }

}
