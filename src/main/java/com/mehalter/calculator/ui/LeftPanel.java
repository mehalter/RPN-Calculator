package com.mehalter.calculator.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.mehalter.calculator.model.CalcState;
import com.mehalter.calculator.model.TextFields;
import com.mehalter.calculator.model.Utils;

public class LeftPanel extends JPanel {

    private static final long serialVersionUID = 6304604086038359864L;

    private JButton[] leftKeys;

    public LeftPanel(CalcState state, Utils utils, TextFields stacks) {
        setLayout(new GridLayout(6, 1));
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(100, 0));
        leftKeys = new JButton[6];
        leftKeys[0] = new SimpleButton("roll");
        leftKeys[0].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (state.stackSize() != 0 && !stacks.get(4).endsWith("E")) {
                    state.setEdit(false);
                    state.setOped(false);
                    if (stacks.get(4).equals(""))
                        stacks.setText(4, "0");
                    state.pushStack(Double.parseDouble(stacks.get(4)));
                    Double[] oldStack = new Double[state.stackSize()];
                    int m = 0;
                    for (double i : state.getStack()) {
                        oldStack[m] = i;
                        m++;
                    }
                    state.clearStack();
                    ;
                    state.pushStack(oldStack[0]);
                    for (int i = oldStack.length - 1; i > 0; i--)
                        state.pushStack(oldStack[i]);
                    stacks.setText(4, state.popStack().toString());
                    utils.refreshStack();
                }
            }
        });
        leftKeys[1] = new SimpleButton("swap");
        leftKeys[1].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (state.stackSize() >= 1 && !stacks.get(4).endsWith("E")) {
                    state.setEdit(false);
                    state.setOped(false);
                    if (stacks.get(4).equals(""))
                        stacks.setText(4, "0");
                    String x = state.popStack().toString();
                    state.pushStack(Double.parseDouble(stacks.get(4)));
                    stacks.setText(4, x);
                    utils.refreshStack();
                }
            }
        });
        leftKeys[2] = new SimpleButton("drop");
        leftKeys[2].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                state.setEdit(false);
                state.setOped(false);
                stacks.setText(4, state.stackSize() == 0 ? "" : state
                        .popStack().toString());
                utils.refreshStack();
            }
        });
        leftKeys[3] = new SimpleButton("AC");
        leftKeys[3].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                state.setEdit(false);
                state.setOped(false);
                state.clearStack();
                stacks.setText(4, "");
                utils.refreshStack();
            }
        });
        leftKeys[4] = new SimpleButton("C");
        leftKeys[4].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                state.setEdit(false);
                state.setOped(false);
                stacks.setText(4, "");
            }
        });
        leftKeys[5] = new SimpleButton("del");
        leftKeys[5].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                state.setEdit(false);
                state.setOped(false);
                if (stacks.get(4).length() == 2 && stacks.get(4).contains("-"))
                    stacks.setText(
                            4,
                            stacks.get(4).substring(0,
                                    stacks.get(4).length() - 2));
                else if (stacks.get(4).length() > 0)
                    stacks.setText(
                            4,
                            stacks.get(4).substring(0,
                                    stacks.get(4).length() - 1));
            }
        });
        for (int k = 0; k < leftKeys.length; k++)
            add(leftKeys[k]);
    }

    public JButton[] getKeys() {
        return leftKeys;
    }

}
