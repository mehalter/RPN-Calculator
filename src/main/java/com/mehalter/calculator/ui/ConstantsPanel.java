package com.mehalter.calculator.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.mehalter.calculator.model.CalcState;
import com.mehalter.calculator.model.TextFields;
import com.mehalter.calculator.model.Utils;

public class ConstantsPanel extends JPanel {

    private static final long serialVersionUID = 8150202400626370885L;

    private TextFields stacks;
    private Utils utils;

    public ConstantsPanel(TextFields stacks, CalcState state, Utils utils) {
        this.stacks = stacks;
        this.utils = utils;
        setLayout(new GridLayout(1, 5));
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(0, 50));
        JButton[] knownKeys = new JButton[5];
        knownKeys[0] = new SimpleButton("\u03C0");
        knownKeys[0].addActionListener(new constantListener(knownKeys[0],
                Double.toString(Math.PI)));
        knownKeys[1] = new SimpleButton("\u2107");
        knownKeys[1].addActionListener(new constantListener(knownKeys[1],
                Double.toString(Math.E)));
        knownKeys[2] = new SimpleButton("sin");
        knownKeys[2].addActionListener(new trigListener(knownKeys[2], stacks,
                utils, state));
        knownKeys[3] = new SimpleButton("cos");
        knownKeys[3].addActionListener(new trigListener(knownKeys[3], stacks,
                utils, state));
        knownKeys[4] = new SimpleButton("tan");
        knownKeys[4].addActionListener(new trigListener(knownKeys[4], stacks,
                utils, state));
        for (int k = 0; k < knownKeys.length; k++)
            add(knownKeys[k]);
    }

    class constantListener implements ActionListener {

        JButton key;
        String constant;

        public constantListener(JButton key, String constant) {
            this.key = key;
            this.constant = constant;

        }

        public void actionPerformed(ActionEvent e) {
            utils.ifEditifOp();
            stacks.setText(4, constant);
        }

    }

}