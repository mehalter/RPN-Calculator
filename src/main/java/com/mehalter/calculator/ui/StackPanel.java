package com.mehalter.calculator.ui;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JPanel;

import com.mehalter.calculator.model.TextFields;

public class StackPanel extends JPanel {

    private static final long serialVersionUID = -8261785989286539516L;

    public StackPanel(TextFields stacks) {
        setLayout(new GridLayout(5, 1));
        setBackground(Color.WHITE);
        for (int i = 0; i < 5; i++) {
            add(stacks.getField(i));
        }
    }

}
