package com.mehalter.calculator.ui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.mehalter.calculator.model.TextFields;
import com.mehalter.calculator.model.Utils;

public class NumPanel extends JPanel {

    private static final long serialVersionUID = 7601340040994175016L;

    private TextFields stacks;
    private Utils utils;

    public NumPanel(TextFields stacks, Utils utils) {
        this.stacks = stacks;
        this.utils = utils;
        Color numColor = new Color(-1118482);

        setLayout(new GridLayout(4, 3));
        setBackground(Color.WHITE);
        JButton[][] numKeys = new JButton[4][3];
        for (int k = 2; k >= 0; k--) {
            for (int l = 0; l < 3; l++) {
                numKeys[k][l] = new SimpleButton("" + (3 * k + l + 1), numColor);
                numKeys[k][l].addActionListener(new numListener(numKeys[k][l]));
                add(numKeys[k][l]);
            }
        }
        numKeys[3][0] = new SimpleButton("0", numColor);
        numKeys[3][0].addActionListener(new numListener(numKeys[3][0]));
        add(numKeys[3][0]);
        numKeys[3][1] = new SimpleButton(".", numColor);
        numKeys[3][1]
                .addActionListener(e -> {
                    if (!stacks.get(4).contains("."))
                        stacks.setText(4, (stacks.get(4).equals("")) ? "0."
                                : stacks.get(4) + ".");
                });
        add(numKeys[3][1]);
        numKeys[3][2] = new SimpleButton("-", numColor);
        numKeys[3][2].addActionListener(e -> {
            if (stacks.get(4).length() != 0)
                stacks.setText(4, (stacks.get(4).charAt(0) == '-') ? stacks
                        .get(4).substring(1) : ('-' + stacks.get(4)));
        });
        add(numKeys[3][2]);
    }

    class numListener implements ActionListener {

        JButton key;

        public numListener(JButton _key) {
            key = _key;
        }

        public void actionPerformed(ActionEvent e) {
            utils.ifEditifOp();
            stacks.setText(4, stacks.get(4) + key.getText());
        }

    }

}
