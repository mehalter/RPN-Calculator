package com.mehalter.calculator.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.mehalter.calculator.model.CalcState;
import com.mehalter.calculator.model.TextFields;
import com.mehalter.calculator.model.Utils;

public class OpPanel extends JPanel {

    private static final long serialVersionUID = 5888535726118050131L;

    private CalcState state;
    private Utils utils;
    private TextFields stacks;
    private JButton[] opKeys;

    public OpPanel(CalcState state, Utils utils, TextFields stacks) {
        this.state = state;
        this.utils = utils;
        this.stacks = stacks;
        setLayout(new GridLayout(4, 3));
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(265, 0));
        opKeys = new JButton[12];
        opKeys[0] = new SimpleButton("<html>log<sub>y</sub></html>");
        opKeys[0].addActionListener(new opListener(opKeys[0]));
        opKeys[1] = new SimpleButton("log");
        opKeys[1].addActionListener(new opListener(opKeys[1]));
        opKeys[2] = new SimpleButton("ln");
        opKeys[2].addActionListener(new opListener(opKeys[2]));
        opKeys[3] = new SimpleButton("\u2713" + "x");
        opKeys[3].addActionListener(new opListener(opKeys[3]));
        opKeys[4] = new SimpleButton("<html>y<sup>x</sup></html>");
        opKeys[4].addActionListener(new opListener(opKeys[4]));
        opKeys[5] = new SimpleButton("<html>e<sup>x</sup></html>");
        opKeys[5].addActionListener(new opListener(opKeys[5]));
        opKeys[6] = new SimpleButton("E");
        opKeys[6].addActionListener(e -> {
            if (!stacks.get(4).contains("E") && !stacks.get(4).isEmpty()) {
                String editText = stacks.get(4);
                utils.ifEditifOp();
                stacks.setText(4, editText + "E");
            }
        });
        opKeys[7] = new SimpleButton("*");
        opKeys[7].addActionListener(new opListener(opKeys[7]));
        opKeys[8] = new SimpleButton("/");
        opKeys[8].addActionListener(new opListener(opKeys[8]));
        opKeys[9] = new SimpleButton("Ent");
        opKeys[9].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!stacks.get(4).endsWith("E")) {
                    if (stacks.get(4).isEmpty()) {
                        state.pushStack(new Double(0));
                        stacks.setText(4, "0");
                    } else if (Utils.validEnter(stacks.get(4)))
                        stacks.setText(4, "");
                    else
                        state.pushStack(Double.parseDouble(stacks.get(4)));
                    for (int i = 0; i < 3; i++) {
                        stacks.setText(i, stacks.get(i + 1));
                        stacks.setText(i + 1, "");
                    }
                    stacks.setText(3, stacks.get(4));
                    stacks.setText(4, "");
                    utils.refreshStack();
                    stacks.setText(4, state.peekStack().toString());
                    state.setOped(false);
                    state.setEdit(true);
                }
            }
        });
        opKeys[10] = new SimpleButton("+");
        opKeys[10].addActionListener(new opListener(opKeys[10]));
        opKeys[11] = new SimpleButton("-");
        opKeys[11].addActionListener(new opListener(opKeys[11]));
        for (int k = 0; k < opKeys.length; k++)
            add(opKeys[k]);
    }

    public JButton[] getKeys() {
        return opKeys;
    }

    class opListener implements ActionListener {

        JButton click;
        Double ans;

        public opListener(JButton _click) {
            click = _click;
        }

        public void actionPerformed(ActionEvent e) {

            if (state.stackSize() >= 1 && !stacks.get(4).endsWith("E")) {
                if (stacks.get(4).equals(""))
                    stacks.setText(4, "0");
                state.pushStack(Double.parseDouble(stacks.get(4)));
                double x = state.popStack();
                double y = state.popStack();
                switch (click.getText()) {
                case "+":
                    ans = y + x;
                    break;
                case "-":
                    ans = y - x;
                    break;
                case "*":
                    ans = y * x;
                    break;
                case "/":
                    ans = y / x;
                    break;
                case "<html>y<sup>x</sup></html>":
                    ans = Math.pow(y, x);
                    break;
                case "<html>e<sup>x</sup></html>":
                    ans = Math.pow(Math.E, x);
                    state.pushStack(y);
                    break;
                case "\u2713" + "x":
                    ans = Math.sqrt(x);
                    state.pushStack(y);
                    break;
                case "<html>log<sub>y</sub></html>":
                    ans = Math.log(x) / Math.log(y);
                    break;
                case "log":
                    ans = Math.log10(x);
                    state.pushStack(y);
                    break;
                case "ln":
                    ans = Math.log(x);
                    state.pushStack(y);
                    break;
                }
                stacks.setText(4, ans.toString());
                utils.refreshStack();
                state.setOped(true);
                state.setEdit(true);
            } else {
                if (click.getText() == "\u2713" + "x"
                        || click.getText() == "log" || click.getText() == "ln"
                        || click.getText() == "<html>e<sup>x</sup></html>"
                        && !stacks.get(4).endsWith("E")) {
                    if (stacks.get(4).equals(""))
                        stacks.setText(4, "0");
                    state.pushStack(Double.parseDouble(stacks.get(4)));
                    switch (click.getText()) {
                    case "\u2713" + "x":
                        ans = Math.sqrt(state.popStack());
                        break;
                    case "log":
                        ans = Math.log10(state.popStack());
                        break;
                    case "ln":
                        ans = Math.log(state.popStack());
                        break;
                    case "<html>e<sup>x</sup></html>":
                        ans = Math.pow(Math.E, state.popStack());
                        break;
                    }
                    stacks.setText(4, ans.toString());
                    utils.refreshStack();
                    state.setOped(true);
                    state.setEdit(true);
                }
            }

        }
    }
}