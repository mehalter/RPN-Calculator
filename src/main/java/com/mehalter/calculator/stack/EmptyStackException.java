package com.mehalter.calculator.stack;

@SuppressWarnings("serial")
public class EmptyStackException extends RuntimeException {

    public EmptyStackException() {
        super();
    }

    public EmptyStackException(String s) {
        super(s);
    }

    public EmptyStackException(String s, Throwable t) {
        super(s, t);
    }

    public EmptyStackException(Throwable t) {
        super(t);
    }

}
