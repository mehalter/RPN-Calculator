package com.mehalter.calculator.stack;

import java.util.Iterator;

public interface IStack<E> extends Iterable<E> {

    /**
     * This will tell if the stack is empty
     * 
     * @return true if the stack is empty and false otherwise
     */
    public boolean isEmpty();

    /**
     * This will discard all items on the stack
     */
    public void clear();

    /**
     * This puts <code>newItem</code> on the stack
     * 
     * @param newItem
     *            the new object we are placing on the stack
     */
    public void push(E newItem);

    /**
     * This removes and returns the item on the top of the stack
     * 
     * @return item on the top of the stack
     * @throws an
     *             <code>EmpyStackException</code> if you try to pop from an
     *             empty stack.
     */
    public E pop();

    /**
     * This returns the item on the top of the stack
     * 
     * @return item on the top of the stack
     * @throws an
     *             <code>EmpyStackException</code> if you try to peek from an
     *             empty stack.
     */
    public E peek();

    /**
     * This will let the user know how many elements are in the stack
     * 
     * @return an int representing the stack's size.
     */
    public int size();

    public Iterator<E> iterator();

}
