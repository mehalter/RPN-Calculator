package com.mehalter.calculator.stack;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LStack<E> implements IStack<E> {

    private Link<E> top;

    public LStack() {
        top = null;
    }

    @Override
    public String toString() {
        if (isEmpty())
            return "[]";
        Link<E> nav = top;
        StringBuffer out = new StringBuffer();
        out.append("[");
        while (nav.getSize() > 1) {
            out.append(nav.getE());
            out.append(", ");
            nav = nav.getLink();
        }
        out.append(nav.getE() + "]");
        return out.toString();
    }

    public int size() {
        return isEmpty() ? 0 : top.getSize();
    }

    public boolean isEmpty() {
        return top == null;
    }

    public void clear() {
        top = null;
    }

    public void push(E newItem) {
        top = new Link<E>(newItem, top);
    }

    public E pop() {
        if (isEmpty())
            throw new EmptyStackException("Cannot pop from an empty stack");
        E out = top.getE();
        top = top.getLink();
        return out;
    }

    public E peek() {
        if (isEmpty())
            throw new EmptyStackException("Cannot peek into an empty stack");
        return top.getE();
    }

    @Override
    public Iterator<E> iterator() {
        return new LTraveler<E>(top);
    }

}

class LTraveler<E> implements Iterator<E> {

    private Link<E> iTop;

    public LTraveler(Link<E> iTop) {
        this.iTop = iTop;
    }

    @Override
    public boolean hasNext() {
        return iTop != null;
    }

    @Override
    public E next() {
        if (!hasNext())
            throw new NoSuchElementException();
        E out = iTop.getE();
        iTop = iTop.getLink();
        return out;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}

class Link<E> {

    private final E item;
    private final Link<E> next;
    private final int size;

    public Link(E item, Link<E> next) {
        this.item = item;
        this.next = next;
        this.size = next == null ? 1 : next.getSize() + 1;
    }

    public E getE() {
        return this.item;
    }

    public Link<E> getLink() {
        return this.next;
    }

    public int getSize() {
        return this.size;
    }

}